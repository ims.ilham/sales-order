package org.acme.controller;


import org.acme.service.ProductService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/api/v1/sorder/product")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductControllers {

    @Inject
    ProductService service;

    @POST
    public String getAllData(){
        return "GETALL";
    }

    @POST
    public String postData(){
        return "POST";
    }

    @POST
    public String putData(){
        return "PUT";
    }

    @POST
    public String deleteData(){
        return  "DELETE";
    }
}
