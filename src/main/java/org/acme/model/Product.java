package org.acme.model;

import io.smallrye.common.constraint.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "product")
public class Product {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull
    @Column(name = "name", nullable = false)
    private String description;

    private Double price;
}
