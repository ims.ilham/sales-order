package org.acme.model;

import io.smallrye.common.constraint.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "customer")
public class Customer {

    private static final long serialVersionUID = 1L;

    @NotNull
    @Column(name = "name", length = 50, nullable = false)
    private String name;

    @NotNull
    @Column(name = "phone", length = 50, nullable = false)
    private String phone;

    @NotNull
    @Column(name = "address", length = 200, nullable = false)
    private String address;

}
