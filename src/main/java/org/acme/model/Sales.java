package org.acme.model;

import io.smallrye.common.constraint.NotNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "sales")
public class Sales{

    private static final long serialVersionUID = 1L;

    private Date date;

    @NotNull
    @Column(name = "name", length = 20, nullable = false)
    private String transaction_number;

    @NotNull
    @Column(name = "name", nullable = false)
    private Integer customerid;
}
